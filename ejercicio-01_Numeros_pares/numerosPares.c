/*	Programa: numerosPares.c
	Programador: Aparicio Zu�iga Berenice
	Descripci�n: Elabora un programa que imprima los n�meros pares del 0 al 100.
	Fecha de creaci�n: 20, Octubre 2022
	�ltima modificaci�n: 20, Octubre 2022
*/

#include <stdio.h>

int main(){
	//declaraci�n de variable
	int i;
	
	for(i = 0; i<=100; i++ ){
		if(i%2==0){
			printf("%i. \n", i);
		}	
	}
	return 0;
}
