/*	Programa: ejercicio-02_suma_consecutivos.c
	Programador: Aparicio Z��iga Berenice
	Descripci�n: Elabora un programa que reciba un n�mero entre 1 y 50 y devuelva la suma de los n�meros consecutivos del 1 hasta ese n�mero.
	Fecha de creaci�n: 21, Octubre 2022
	�ltima modificaci�n: 21, Octubre 2022
*/

#include <stdio.h>	

int main(){
	//declaraci�n de variable
	int numero=0, suma=0, i=0;
	
	//DATOS DE ENTRADA
	
	do{
		printf("Introduzca un numero entre 1 y 50: \n");
		scanf("%i", &numero);
		if(numero>=1 && numero<=50){
			for(i=1; i<=numero; i++){
				suma = suma + i;
			}
			printf("\nLa suma de los numeros consecutivos es: %i", suma); 
		}
	}while(numero >50);
	
	return 0;
}
