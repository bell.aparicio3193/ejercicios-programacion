/*	Programa: dolaresPesos.c
	Programador: Aparicio Z��iga Berenice
	Descripci�n: Elabora un programa que reciba como entrada un monto en d�lares (USD) y devuelva la cantidad equivalente en pesos mexicanos (MXN)
	Fecha de creaci�n: 20, Octubre 2022
	�ltima modificaci�n: 20, Octubre 2022
*/

#include <stdio.h>
#define VALORDOLAR 20.04;

int main(){
	//declaraci�n de variable
	float montoDolares = 0, pesosMexicanos = 0;
	
	//DATOS DE ENTRADA
	
	printf("Ingrese el monto en dolares: ");
	scanf("%f", &montoDolares);
	
	//PROCESO
	
	pesosMexicanos = montoDolares * VALORDOLAR;
	
	//DATOS DE SALIDA
	
	printf("\nEl monto equivalente en pesos mexicanos es: $%.2f", pesosMexicanos);
	
	return 0;
}
