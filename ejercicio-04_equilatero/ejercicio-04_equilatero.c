/*	Programa: ejercicio-04_equilatero.c
	Programador: Aparicio Z��iga Berenice
	Descripci�n: Elabora un programa calcule el per�metro de un tri�ngulo equil�tero. El programa pedir� al usuario las entradas necesarias.
	Fecha de creaci�n: 20, Octubre 2022
	�ltima modificaci�n: 20, Octubre 2022
*/

#include <stdio.h>

int main(){
	//declaraci�n de variable
	float lados = 0, perimetro = 0;
	
	//DATOS DE ENTRADA
	
	printf("Ingrese la longitud de uno de los lados: ");
	scanf("%f", &lados);
	
	//PROCESO
	
	perimetro = 3 * lados;
	
	//DATOS DE SALIDA
	
	printf("\nEl perimetro del triangulo equilatero es: %.1f", perimetro);
	
	return 0;
}
