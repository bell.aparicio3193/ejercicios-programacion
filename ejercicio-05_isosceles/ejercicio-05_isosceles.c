/*	Programa: ejercicio-05_isosceles.c
	Programador: Aparicio Z��iga Berenice
	Descripci�n: Elabora un programa que calcule el per�metro de un tri�ngulo is�sceles. El programa pedir� al usuario las entradas necesarias.
	Fecha de creaci�n: 20, Octubre 2022
	�ltima modificaci�n: 20, Octubre 2022
*/

#include <stdio.h>

int main(){
	//declaraci�n de variable
	float ladosIguales = 0, ladoDiferentes = 0, perimetro = 0;
	
	//DATOS DE ENTRADA
	
	printf("Ingrese la longitud de uno de los lados iguales: ");
	scanf("%f", &ladosIguales);
	
	printf("Ingrese la longitud de uno del lado diferente: ");
	scanf("%f", &ladoDiferentes);
	
	//PROCESO
	
	perimetro = (2*ladosIguales) + ladoDiferentes;
	
	//DATOS DE SALIDA
	
	printf("\nEl perimetro del triangulo isosceles es: %.1f", perimetro);
	
	return 0;
}
