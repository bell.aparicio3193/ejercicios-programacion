/*	Programa: ejercicio-06_suma_escaleno.c
	Programador: Aparicio Z��iga Berenice
	Descripci�n: Elabora un programa que calcule el per�metro de un tri�ngulo escaleno. El programa pedir� al usuario las entradas necesarias.
	Fecha de creaci�n: 20, Octubre 2022
	�ltima modificaci�n: 20, Octubre 2022
*/

#include <stdio.h>

int main(){
	//declaraci�n de variable
	float ladoLateral = 0, ladoBase=0, ladoOpuesto=0, perimetro = 0;
	
	//DATOS DE ENTRADA
	
	printf("Introduzca la longitud de la base: ");
	scanf("%f", &ladoBase);
	
	printf("Introduzca la longitud del lado lateral: ");
	scanf("%f", &ladoLateral);
	
		printf("Introduzca la longitud del lado opuesto: ");
	scanf("%f", &ladoOpuesto);
	
	//PROCESO
	
	perimetro = ladoBase + ladoLateral + ladoOpuesto;
	
	//DATOS DE SALIDA
	
	printf("\nEl perimetro del triangulo escaleno es: %.1f", perimetro);
	
	return 0;
}
