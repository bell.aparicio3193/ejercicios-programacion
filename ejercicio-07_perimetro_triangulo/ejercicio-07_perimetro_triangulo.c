/*	Programa: ejercicio-07_perimetro_triangulo.c
	Programador: Aparicio Z��iga Berenice
	Descripci�n: Elabora programa que calcule el per�metro de un tri�ngulo. 
	El programa preguntar� al usuario el tipo de tri�ngulo (equil�tero, is�sceles o escaleno) y 
	le pedir� las entradas necesarias para realizar el c�lculo necesario.
	Fecha de creaci�n: 20, Octubre 2022
	�ltima modificaci�n: 20, Octubre 2022
*/

#include <stdio.h>

int main(){
	//declaraci�n de variable
	float ladoLateral = 0, ladoBase=0, ladoOpuesto=0, perimetro = 0;
	
	//DATOS DE ENTRADA
	
	printf("Introduzca la longitud de la base: ");
	scanf("%f", &ladoBase);
	
	printf("Introduzca la longitud del lado lateral: ");
	scanf("%f", &ladoLateral);
	
		printf("Introduzca la longitud del lado opuesto: ");
	scanf("%f", &ladoOpuesto);
	
	//PROCESO
	
	perimetro = ladoBase + ladoLateral + ladoOpuesto;
	
	//DATOS DE SALIDA
	
	printf("\nEl perimetro del triangulo escaleno es: %.1f", perimetro);
	
	return 0;
}
