/*	Programa: ejercicio-08_tablas.c
	Programador: Aparicio Z��iga Berenice
	Descripci�n: Elabora un programa que reciba un n�mero e imprima la tabla de multiplicar de ese n�mero del 2 al 10. 
	Fecha de creaci�n: 20, Octubre 2022
	�ltima modificaci�n: 20, Octubre 2022
*/

#include <stdio.h>	

int main(){
	//declaraci�n de variable
	int numero, i;
	
	//DATOS DE ENTRADA
	
	printf("Introduzca un numero para mostrar la tabla de multiplicar: ");
	scanf("%i", &numero);
		
	//PROCESO
	
	for(i=2; i<=10; i++){
		
		//DATOS DE SALIDA
		printf("\n%i x %i = %i", numero, i, (numero * i ));
	}
	
	return 0;
}
