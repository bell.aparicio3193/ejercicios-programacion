/*	Programa: ejercicio-09_desigualdad_triangulo.c
	Programador: Aparicio Z��iga Berenice
	Descripci�n: Teorema de la desigualdad de un tri�ngulo. Si tenemos tres segmentos de cualquier longitud, no siempre se puede construir un tri�ngulo con ellos. 
	Elabora un programa que reciba la longitud de 3 segmentos y determine si con ellos es posible formar un tri�ngulo.
	Fecha de creaci�n: 21, Octubre 2022
	�ltima modificaci�n: 21, Octubre 2022
*/

#include <stdio.h>	

int main(){
	//declaraci�n de variable
	int lado1=0, lado2=0, lado3=0;
	
	//DATOS DE ENTRADA
	
	printf("Introduzca el primer segmento: ");
	scanf("%i", &lado1);
	printf("Introduzca el segundo segmento: ");
	scanf("%i", &lado2);
	printf("Introduzca el tercer segmento: ");
	scanf("%i", &lado3);
		
	//PROCESO
	
	if(lado1+lado2>lado3 && lado1+lado3>lado2 && lado2+lado3>lado1){
		//DATOS DE SALIDA
		printf("Si se cumplen las 3 desigualdades entonces si es posible construir un triangulo con los segmentos primer, segundo y tercero.");
	}else{
		printf("No se cumplen las 3 desigualdades entonces no es posible construir un triangulo con los segmentos primer, segundo y tercero.");
	}
	
	return 0;
}
