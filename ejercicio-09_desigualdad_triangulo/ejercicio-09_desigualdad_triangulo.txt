Proceso DesigualdadTriangulo
	
	//Problema: Teorema de la desigualdad de un tri�ngulo. Si tenemos tres segmentos de cualquier longitud, no siempre se puede construir un tri�ngulo con ellos. 
	//Elabora un programa que reciba la longitud de 3 segmentos y determine si con ellos es posible formar un tri�ngulo.
	//Autor: Berenice Aparicio Z��iga
	
	//VARIABLES
	Definir lado1, lado2, lado3 Como Entero;
	
	//DATOS DE ENTRADA
	Escribir "Introduce el primer segmento: ";
	Leer lado1;
	Escribir "Introduce el segundo segmento: ";
	Leer lado2;
	Escribir "Introduce el tercer segmento: ";
	Leer lado3;
	
	//PROCESO
	Si lado1+lado2>lado3 && lado1+lado3>lado2 && lado2+lado3>lado1 Entonces
		Escribir "Si se cumplen las 3 desigualdades entonces si es posible construir un tri�ngulo con los segmentos primer, segundo y tercero.";
	SiNo
		Escribir "No se cumplen las 3 desigualdades entonces no es posible construir un tri�ngulo con los segmentos primer, segundo y tercero.";
	Fin Si
FinProceso
