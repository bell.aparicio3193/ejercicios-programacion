/*	Programa: ejercicio-10_es_bisiesto.c
	Programador: Aparicio Z��iga Berenice
	Descripci�n: Elabora un programa o una funci�n que reciba un n�mero entero que represente un a�o y devuelva verdadero si el a�o es bisiesto.
	Fecha de creaci�n: 21, Octubre 2022
	�ltima modificaci�n: 21, Octubre 2022
*/

#include <stdio.h>	

void fnAgnoBisiesto(int agno);

int main(){
	//declaraci�n de variable
	int agnoBisiesto;
	
	//DATOS DE ENTRADA
	
	printf("Introduzca el agno para saber si es bisiesto: ");
	scanf("%d", &agnoBisiesto);
	
	//mandando a llamar a la funci�n
	fnAgnoBisiesto(agnoBisiesto);
	
	
}//fin

//FUNCION PARA CONOCER SI EL A�O ES BISIESTO

void fnAgnoBisiesto(int agno){
	
	if(agno% 4==0){
		printf("\nV E R D A D E R O.");
	}else{
		printf("\nF A L S O.");
	}
}
