/*	Programa: ejercicio-11_meses.c
	Programador: Aparicio Z��iga Berenice
	Descripci�n: Elabora un programa que reciba un n�mero entero entre 0 y 11, debe devolver el nombre de mes correspondiente. Toma en cuenta que 0 = Enero y 11 = Diciembre.
	Fecha de creaci�n: 21, Octubre 2022
	�ltima modificaci�n: 21, Octubre 2022
*/

#include <stdio.h>	

int main(){
	//declaraci�n de variable
	int numeroMes;
	
	//DATOS DE ENTRADA
	do{
		printf("Introduzca un numero para conocer el nombre del mes: \n");
		scanf("%i", &numeroMes);
		
		//PROCESO
		
		numeroMes -=1;
		
		switch(numeroMes){
			case 0: 
				printf("Enero");
			break;
			case 1: 
				printf("Febrero");
			break;
			case 2: 
				printf("Marzo");
			break;
			case 3: 
				printf("Abril");
			break;
			case 4: 
				printf("Mayo");
			break;
			case 5: 
				printf("Junio");
			break;
			case 6: 
				printf("Julio");
			break;
			case 7: 
				printf("Agosto");
			break;
			case 8: 
				printf("Septiembre");
			break;
			case 9: 
				printf("Octubre");
			break;
			case 10: 
				printf("Noviembre");
			break;
			case 11: 
				printf("Diciembre");
			break;
			default: 
				printf("Introduzca la opcion correcta\n");
			break;		
		}
	}while(numeroMes>=12);	

	return 0;
}
