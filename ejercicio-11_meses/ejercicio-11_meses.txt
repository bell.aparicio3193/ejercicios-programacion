Proceso MesesAgno
	// Elabora un programa que reciba un n�mero entero entre 0 y 11, 
	// debe devolver el nombre de mes correspondiente. Toma en cuenta que 0 = Enero y 11 = Diciembre.
	//Autor: Berenice Aparicio Z��iga
	
	//VARIABLES
	Definir numeroMes como Entero;
	
	//DATOS DE ENTRADA
	Escribir "Introduzca un n�mero para conocer el nombre del mes: ";
	Leer numeroMes;
	
	numeroMes=numeroMes - 1;
	
	Segun numeroMes Hacer
		0:
			Escribir "Enero";
		1:
			Escribir "Febrero";
		2:
			Escribir "Marzo";
		3:
			Escribir "Abril";
		4:
			Escribir "Mayo";
		5:
			Escribir "Junio";
		6:
			Escribir "Julio";
		7:
			Escribir "Agosto";
		8:
			Escribir "Septiembre";
		9:
			Escribir "Octubre";
		10:
			Escribir "Noviembre";
		11:
			Escribir "Diciembre";
		De Otro Modo:
			Escribir "Introduzca un n�mero entre el 1 al 12";
	Fin Segun
	
FinProceso