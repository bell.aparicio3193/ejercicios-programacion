/*	Programa: ejercicio-12_fecha_real.c
	Programador: Aparicio Z��iga Berenice
	Descripci�n: Elabora un programa o una funci�n que reciba un d�a, mes y a�o y devuelva verdadero si la fecha es real y falso si la fecha es irreal. 
	Un ejemplo de fecha irreal es 30 de febrero o 31 de abril o 29 de febrero de 2021.
	Fecha de creaci�n: 22, Octubre 2022
	�ltima modificaci�n: 22, Octubre 2022
*/

#include <stdio.h>

int main(){
	
	int dia, mes, agno;
	
	//DATOS DE ENTRADA
	printf("Escriba una fecha 01/1/1993\n") ;
	printf("Introduzca el dia:");
	scanf("%i", &dia);
	printf("Introduzca el mes:");
	scanf("%i", &mes);
	printf("Introduzca el mes:");
	scanf("%i", &agno);

	//PROCESO
	
	if((dia > 31 || dia <=0) || (mes <=0 || mes >12) || (agno %4 != 0 && mes== 2 && dia>28) || agno <=0){
		
		printf("F A L S O ");
	
	}else if((mes==1 && dia<31) || (mes==3 && dia<31) || (mes==5 && dia<31) || (mes==7 && dia<31) || (mes==8 && dia<31) || (mes==12 && dia<31) || (mes==10 && dia<31)){
		
		printf("F A L S O ");
	}
	else if((mes==4 && dia<30) || (mes==6 && dia<30) || (mes==9 && dia<30) || (mes==11 && dia<30)){
		
		printf("F A L S O ");
	}
	else if((mes==4 && dia>30) || (mes==6 && dia>30) || (mes==9 && dia>30) || (mes==11 && dia>30)){
		printf("F A L S O ");
	}
	else{
		printf("V E R D A D E R O ");
	}
	
	return 0;
}

