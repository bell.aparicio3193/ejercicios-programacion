Proceso FechasReales
	//Elabora un programa o una funci�n que reciba un d�a, mes y a�o y devuelva verdadero si la fecha es real y falso si la fecha es irreal. 
	//Un ejemplo de fecha irreal es 30 de febrero o 31 de abril o 29 de febrero de 2021.
	//Autor: Berenice Aparicio Z��iga
	
	Definir dia, mes, agno como Entero;
	
	//DATOS DE ENTRADA
	Escribir "Escriba una fecha 01/1/1993";
	Escribir "Introduzca el d�a:";
	Leer dia;
	Escribir "Introduzca el mes:";
	Leer mes;
	Escribir "Introduzca el a�o:";
	Leer agno;
	
	Si (dia > 31 o dia <=0) Y (mes <=0 o mes >12) Y (agno %4 <> 0 Y mes== 2 y dia>28) o agno<=0 Entonces
		Escribir "Falso";
	SiNo
		Si (mes==1 Y dia<31) o (mes==3 Y dia<31) o (mes==5 Y dia<31) o (mes==7 Y dia<31) o (mes==8 Y dia<31) o (mes==12 Y dia<31)o (mes==10 Y dia<31)Entonces
			Escribir "Falso";
		SiNo
			Si (mes==4 Y dia<30) o (mes==6 Y dia<30) o (mes==9 Y dia<30) o (mes==11 Y dia<30)  Entonces
				Escribir "Falso";
			SiNo
				Si (mes==4 Y dia>30) o (mes==6 Y dia>30) o (mes==9 Y dia>30) o (mes==11 Y dia>30) Entonces
					Escribir "Falso";
				SiNo
					Escribir "Verdadero";
				Fin Si
			Fin Si
		Fin Si
	Fin Si
FinProceso
