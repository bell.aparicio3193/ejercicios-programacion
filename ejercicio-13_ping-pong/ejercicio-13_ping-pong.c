/*	Programa: ejercicio-13_ping-pong.c
	Programador: Aparicio Z��iga Berenice
	Descripci�n: 
	Escribe un programa que imprima los n�meros del 1 al 100, pero aplicando las siguientes reglas.
	Regla 1: Cuando el n�mero sea divisible entre 3, en vez del n�mero debe escribir "ping"
	Regla 2: Cuando el n�mero sea divisible entre 5, en vez del n�mero debe escribir "pong"
	Regla 3: Cuando el n�mero sea divisible entre 3 y tambi�n divisible entre 5, en vez del n�mero debe escribir "ping-pong"
	Fecha de creaci�n: 21, Octubre 2022
	�ltima modificaci�n: 21, Octubre 2022
*/

#include <stdio.h>	

int main(){
	//declaraci�n de variable
	int i;
	
	//PROCESO
	
	for (i=1; i<=100; i++){
		if(i % 3 == 0 && i % 5 == 0) {
			printf("Ping-pong\n");	
		} 
		else if (i % 5 == 0){
			printf("Pong\n");
		} 
		else if (i % 3 == 0){
			printf("Ping\n");
		} 		
	}
	return 0;
}
