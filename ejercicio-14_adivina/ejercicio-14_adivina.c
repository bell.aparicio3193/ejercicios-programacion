/*	Programa: ejercicio-14_adivinao.c
	Programador: Aparicio Z��iga Berenice
	Descripci�n: Problema: Elabora un programa en el cual el usuario debe adivinar un n�mero entre 1 y 100. 
	El n�mero es generado aleatoriamente por el programa. El programa solo le dar� 5 intentos al usuario para adivinar. 
	Al final de los 5 intentos, el programa deber� mostrar el mensaje "PERDISTE" y mostrar� cu�l era el n�mero correcto.
	Si el usuario adivina en alguno de los 5 intentos, el programa debe mostrar el mensaje "GANASTE" y mostrar� cu�l es el n�mero.
	Para efectos de dise�o, supongamos que existe una funci�n llamada "random(desde, hasta)" que genera un n�mero entero aleatorio
	delimitado por los par�metros "desde" y "hasta", los cuales son n�meros enteros.
	Fecha de creaci�n: 22, Octubre 2022
	�ltima modificaci�n: 22, Octubre 2022
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int random();

int main(){
	
	int numeroUsuario = 0;
	int intentos = 5;
	int numeroRandom = random();
	
	do{
		printf("Escriba un n%cmero entre 1 y 100: \n", 163);
		scanf( "%i", &numeroUsuario);
		intentos = intentos - 1;
		
	}while(numeroRandom != numeroUsuario && intentos>0);
	
	if(numeroRandom != numeroUsuario){
		printf("\nP E R D I S T E ");
	
	}else {
		printf("\nG A N A S T E ");
	}
	
	printf("\nEl numero es: %i", numeroRandom);
	
	return 0;
}
int random (){
	//PARA GENERAR NUMEROS ALEATORIOS DISTINTOS
	srand(time(NULL));
	int numAleatorio = rand() % 101;
	return numAleatorio;
}
